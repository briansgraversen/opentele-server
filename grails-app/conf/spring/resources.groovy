import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils
import org.opentele.server.UserDetailsService
import org.opentele.server.constants.OpenteleAuditLogLookup
import org.opentele.server.util.*
import org.springframework.security.core.session.SessionRegistryImpl
import org.springframework.security.web.authentication.session.ConcurrentSessionControlStrategy
import org.springframework.security.web.session.ConcurrentSessionFilter
import org.springframework.web.servlet.i18n.SessionLocaleResolver
import wslite.http.HTTPClient
import wslite.soap.SOAPClient

// Place your Spring DSL code here
beans = {
    localeResolver(SessionLocaleResolver) {
        defaultLocale = grailsApplication.config.defaultLocale
        Locale.setDefault(grailsApplication.config.defaultLocale)

        customPropertyEditorRegistrar(CustomPropertyEditorRegistrar)
        auditLogLookupBean(OpenteleAuditLogLookup)
        userDetailsService(UserDetailsService)
    }

    iuaAuthenticationProvider(IUAAuthenticationProvider) {
        iuaCertificate = 'MIIDlzCCAn+gAwIBAgIENdp6yDANBgkqhkiG9w0BAQsFADB8MQswCQYDVQQGEwJESzEQMA4GA1UECBMHSnlsbGFuZDEPMA0GA1UEBxMGQWFyaHVzMRMwEQYDVQQKEwpIZWFsdGhjYXJlMREwDwYDVQQLEwhTb2Z0d2FyZTEiMCAGA1UEAxMZQWxleGFuZHJhIEluc3RpdHV0dGV0IEEvUzAeFw0xNDA1MjAwODQzMDZaFw0yNDA1MjAwODQzMDZaMHwxCzAJBgNVBAYTAkRLMRAwDgYDVQQIEwdKeWxsYW5kMQ8wDQYDVQQHEwZBYXJodXMxEzARBgNVBAoTCkhlYWx0aGNhcmUxETAPBgNVBAsTCFNvZnR3YXJlMSIwIAYDVQQDExlBbGV4YW5kcmEgSW5zdGl0dXR0ZXQgQS9TMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAq/XhJ6y9tvRydey9uKpO4i4Cflsmbo291BBM44RS77TVGz1/Tyamb2+y36W8v4NM2bc6Oh4hDimhOMr9nvFXy1/nW6XLFErmCj99W+bummNImjXGT3PwIhpzd0lsMhL/hkH+Y2Bik2nc21oeRQ1H7oglZGal2JMDp5qU9cez/7k49S6QbX6ZI88nzZpNwGpXq6/2ANK9ITHTMiVpgD6K1mKYmyZzXUnMIUibiQ5WKMD+aTByoKVSl2ww9Ez/wfQOocpoNaLOiwr7W2z/GXSe/QojSOqOYU4QdsWb/JwZ8qNaV66WXeUyjQOFs9wq+t6qdvx2AMYEz48HHG3jc0pf/wIDAQABoyEwHzAdBgNVHQ4EFgQU3fmyQ68tajtyUmu/g1IsH/CYuxswDQYJKoZIhvcNAQELBQADggEBAJjoReeYWgPZvDZiZ36HVO5HAlk80Py+plH2j9cKXs/68pj0mLxYBy16UVT7jFk9ybVyNvTa1ekjEwwaUwku/5Lc5iVqX0Dh8t8PPUCy5OQoK2Ob4C60XsOC3FtlelVMcGHB7umLt67H/Cnf3lBdu6QqCnswRsM9c6IzLHu7DYES9OR9zRqbiT+RYUlv/KGHURoU5LRI0N4YwXCCQXA+iECrTYYyWEI6ZPRaauVmjR+f6rIytzz8NeX4/qfEIBcNTXw7CN5lDUr3PFRQyS8kZuKSKUtVZJMTE0/2wXSvK+ddPqvD9spNr7Mdrwdj31TQmnc2vrd/jqADNXAKMW+0TPc='
        userDetailsService = ref('userDetailsService')
        passwordEncoder = ref('passwordEncoder')
        userCache = ref('userCache')
        saltSource = ref('saltSource')
        preAuthenticationChecks = ref('preAuthenticationChecks')
        postAuthenticationChecks = ref('postAuthenticationChecks')
        hideUserNotFoundExceptions = SpringSecurityUtils.securityConfig.dao.hideUserNotFoundExceptions
    }

    basicAuthenticationFilter(OpenteleSecurityBasicAuthenticationFilter) {
        authenticationManager = ref('authenticationManager')
        authenticationEntryPoint = ref('basicAuthenticationEntryPoint')
    }

    if (grailsApplication.config.milou.run) {
        milouHttpClient(HTTPClient) {
            connectTimeout = 5000
            readTimeout = 10000
            useCaches = false
            followRedirects = false
            sslTrustAllCerts = true
        }

        milouSoapClient(SOAPClient) {
            serviceURL = grailsApplication.config.milou.serverURL
            httpClient = ref('milouHttpClient')
        }
    }

    sessionRegistry(SessionRegistryImpl)

    sessionAuthenticationStrategy(ConcurrentSessionControlStrategy, sessionRegistry) {
        maximumSessions = -1
    }

    concurrentSessionFilter(ConcurrentSessionFilter){
        sessionRegistry = sessionRegistry
        expiredUrl = '/login/concurrentSession'
    }
}
