package org.opentele.server.util

import java.nio.charset.Charset
import java.security.Signature
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate

import org.apache.commons.codec.binary.Base64
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.userdetails.UserDetails


class IUAAuthenticationProvider extends DaoAuthenticationProvider {
	private static X509Certificate certificate;

	@Autowired	
	public void setIuaCertificate(String iuaCertificate) {
		byte[] raw = Base64.decodeBase64(iuaCertificate);
		ByteArrayInputStream bis = new ByteArrayInputStream(raw);
		CertificateFactory cf = CertificateFactory.getInstance("X.509");
		this.certificate = cf.generateCertificate(bis);
	}

	// TODO: if we change this to extend AbstractUserDetailsAuthenticationProvider instead, then UserDetailsService will not be called
	//       and we do not need to have the actual users existing in OpenTele. This makes sense in the long-run, since we would have
	//       the users 100% outside OpenTele. Then roles will be extracted from the IUA-Token and set as it currently done in
	//       the UserDetailsService
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
		logger.debug("Verifying: " + userDetails.getUsername());
		
		if (authentication.getCredentials() == null) {
			logger.debug("Authentication failed: no credentials provided");

			throw new BadCredentialsException(messages.getMessage(
					"AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"),
					includeDetailsObject ? userDetails : null);
		}

		String presentedToken = authentication.getCredentials().toString()
		String[] parts = presentedToken.split("\\.");

		String header = new String(Base64.decodeBase64(parts[0]));
		String payload = new String(Base64.decodeBase64(parts[1]));
		IUAHeader jwsHeader = IUAUtil.parseHeader(header);
		IUAPayload jwsPayload = IUAUtil.parsePayload(payload);

		Signature sig = null;
		if ("RS256".equals(jwsHeader.getAlg())){
			sig = Signature.getInstance("SHA256withRSA");
		}
		else {
			logger.error("Unknown signature algorithm in token: " + jwsHeader.getAlg());
			
			throw new BadCredentialsException(messages.getMessage(
				"AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"),
				includeDetailsObject ? userDetails : null);
		}
		
		sig.initVerify(certificate);
		sig.update((parts[0] + "." + parts[1]).getBytes(Charset.forName("UTF-8")));

		if (!sig.verify(Base64.decodeBase64(parts[2]))) {
			logger.error("Bad signature on token");
			
			throw new BadCredentialsException(messages.getMessage(
				"AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"),
				includeDetailsObject ? userDetails : null);
		}

		if (System.currentTimeMillis() > jwsPayload.getExp() * 1000) {
			logger.error("Token has expired");
			
			throw new BadCredentialsException(messages.getMessage(
				"AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"),
				includeDetailsObject ? userDetails : null);
		}
		
		if (!"OpenTele".equals(jwsPayload.getAud())) {
			logger.error("Token has not been issued for OpenTele");
			
			throw new BadCredentialsException(messages.getMessage(
				"AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad Credentials"),
				includeDetailsObject ? userDetails : null);
		}

		if (!userDetails.getUsername().equalsIgnoreCase(jwsPayload.getSub())) {
			logger.error("Token issued to '" + jwsPayload.getSub() + "', but username '" + userDetails.getUsername() + "' was presented!");
			
			throw new BadCredentialsException(messages.getMessage(
				"AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"),
				includeDetailsObject ? userDetails : null);
		}

		logger.debug("IUAAuthentication Provider Authenticated User");
    }
}
