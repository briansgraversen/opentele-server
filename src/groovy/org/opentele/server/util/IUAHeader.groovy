package org.opentele.server.util

class IUAHeader {
	private String alg;
	private String kid;
	
	public String getAlg() {
		return alg;
	}

	public void setAlg(String alg) {
		this.alg = alg;
	}
	
	public String getKid() {
		return kid;

	}

	public void setKid(String kid) {
		this.kid = kid;
	}
}
