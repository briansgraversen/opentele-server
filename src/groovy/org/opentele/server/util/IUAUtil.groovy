package org.opentele.server.util

import com.google.gson.Gson;

class IUAUtil {
	public static IUAHeader parseHeader(String input){
		Gson gson = new Gson();
		return gson.fromJson(input, IUAHeader.class);
	}
	
	public static IUAPayload parsePayload(String input){
		Gson gson = new Gson();
		return gson.fromJson(input, IUAPayload.class);
	}
}
